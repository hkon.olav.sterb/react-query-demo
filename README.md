# This is a project to showcase some of the react query functionality

The code showcase some of the functionality of react query and is used to discuss async state management in Knowlage Sharing session at Statkraft.

## This is how to run the project

The project sets up a simple rest api in the backend folder and use vite to set up and run the frontend. Make sure you have node installed and do the following:
### Backend
1. Navigate to the backend in your terminal.
2. Install the required dependencies with `npm install`.
3. Start the development server with `npm run dev`.
4. Open your web browser and visit `http://localhost:3000` to view the rest api.

### Frontend
1. Get STATKRAFT_GITLAB_TOKEN and NPM_FONTAWESOME_TOKEN from the frontend enablement team in Statkraft and set them in your environment
2. Navigate to the frontend in your terminal.
3. Install the required dependencies with `npm install`.
4. Start the development server with `npm run dev`.
5. Open your web browser and visit `http://localhost:5173` to view the application.