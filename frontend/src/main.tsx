import React from 'react';
import ReactDOM from 'react-dom/client';
import Home from './pages/Home.tsx';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import Naive from './pages/Naive.tsx';
import Effect from './pages/Effect.tsx';
import Query from './pages/Query.tsx';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import './index.css';

import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import Todo from './pages/Todo.tsx';

const queryClient = new QueryClient();

const router = createBrowserRouter([
    {
        path: '/',
        element: <Home />,
    },
    {
        path: '/naive/',
        element: <Naive />,
    },
    {
        path: '/effect/',
        element: <Effect />,
    },
    {
        path: '/query/',
        element: <Query />,
    },
    {
        path: '/todo/',
        element: <Todo />,
    },
]);

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <QueryClientProvider client={queryClient}>
            <RouterProvider router={router} />
            <ReactQueryDevtools initialIsOpen={false} />
        </QueryClientProvider>
    </React.StrictMode>,
);
