import { useQuery } from '@tanstack/react-query';
import { getTodos } from '../endpoints/todosEndpoints';

export const useTodos = () => useQuery({ queryKey: ['todo'], queryFn: () => getTodos(), refetchOnWindowFocus: false });
