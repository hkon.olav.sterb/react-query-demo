import { sleep } from '../utils';

export type ABCValue = { value: number };

export const getAbc = async (abc: string): Promise<ABCValue> => {
    await sleep(1000);
    if (abc === 'A') {
        return { value: 1 };
    }
    if (abc === 'B') {
        await sleep(1000);
        return { value: 2 };
    }
    if (abc === 'C') {
        await sleep(2000);
        return { value: 3 };
    }
    throw new Error("It's easy as");
};
