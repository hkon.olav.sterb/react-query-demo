import { z } from 'zod';
import { sleep } from '../utils';

const todoValidator = z.object({
    id: z.number(),
    title: z.string(),
    done: z.boolean(),
});

const todosValidator = z.array(todoValidator);

export type TodoType = z.infer<typeof todoValidator>;

export const getTodos = async () => {
    const response = await fetch('http://localhost:3000/todos');
    const data = await response.json();
    return todosValidator.parse(data);
};

export const addTodo = async (title: string) => {
    const response = await fetch('http://localhost:3000/todos', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            title: title,
            done: false,
        }),
    });
    const data = await response.json();
    await sleep(1000);
    return todoValidator.parse(data);
};

export const updateTodo = async (todo: TodoType) => {
    const { id, ...rest } = todo;
    const response = await fetch(`http://localhost:3000/todos/${todo.id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(rest),
    });
    const data = await response.json();
    await sleep(1000);
    return todoValidator.parse(data);
};

export const deleteTodo = async (id: number) => {
    const response = await fetch(`http://localhost:3000/todos/${id}`, {
        method: 'DELETE',
    });
    await sleep(1000);
    await response.json();
};
