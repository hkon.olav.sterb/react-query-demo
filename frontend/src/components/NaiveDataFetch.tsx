import React from 'react';
import { ABCValue, getAbc } from '../endpoints/abcEndpoints';

type Props = {
    abc: string;
};

const NaiveDataFetch: React.FC<Props> = ({ abc }) => {
    const [data, setData] = React.useState<ABCValue | undefined>(undefined);
    console.log('rerendering');
    getAbc(abc).then((data) => {
        setData(data);
    });

    return (
        <div className='fetching-container'>
            <h1>State: {abc}</h1>
            {<h1>Value: {data?.value}</h1>}
        </div>
    );
};

export default NaiveDataFetch;
