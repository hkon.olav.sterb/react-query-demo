import React from 'react';
import { ABCValue, getAbc } from '../endpoints/abcEndpoints';

type Props = {
    abc: string;
};

const UseEffectDataFetch: React.FC<Props> = ({ abc }) => {
    const [data, setData] = React.useState<ABCValue | undefined>(undefined);
    const [loading, setLoading] = React.useState<boolean>(false);
    const [error, setError] = React.useState<any>(undefined);
    console.log('rerendering');
    React.useEffect(() => {
        setLoading(true);
        setError(undefined);
        getAbc(abc)
            .then((data) => {
                setData(data);
            })
            .catch((error) => {
                setError(error);
            })
            .finally(() => {
                setLoading(false);
            });
    }, [abc]);
    return (
        <div className='fetching-container'>
            <h1>State: {abc}</h1>
            {loading ? <h1>Loading...</h1> : <h1>Value: {data?.value}</h1>}
            {error && <h1>Error: {error.message}</h1>}
        </div>
    );
};

export default UseEffectDataFetch;
