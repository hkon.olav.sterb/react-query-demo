import { useTodos } from '../queries/useTodos';

const TodoCount: React.FC = () => {
    const todoQuery = useTodos();
    return (
        <div>
            <div className='todo-container'>
                <h1>
                    Todo count: {todoQuery.isLoading ? 'Loading...' : todoQuery.data?.length}
                </h1>
            </div>
        </div>
    );
};

export default TodoCount;
