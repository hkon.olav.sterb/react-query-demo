import React from 'react';
import { getAbc } from '../endpoints/abcEndpoints';
import { useQuery } from '@tanstack/react-query';

type Props = {
    abc: string;
};

const UseQueryDataFetch: React.FC<Props> = ({ abc }) => {
    console.log('rerendering');
    const abcQuery = useQuery({ queryKey: ['abc', abc], queryFn: () => getAbc(abc) });
    return (
        <div className='fetching-container'>
            <h1>State: {abc}</h1>
            {abcQuery.isLoading ? <h1>Loading...</h1> : <h1>Value: {abcQuery.data?.value}</h1>}
            {abcQuery.isError && <h1>Error: {abcQuery.error.message}</h1>}
            {abcQuery.isFetching && <h1>Fetching...</h1>}
        </div>
    );
};

export default UseQueryDataFetch;
