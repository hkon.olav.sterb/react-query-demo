import { SkButton } from '@statkraft/design-system-react';

type Props = {
    setAbc: (abc: string) => void;
};

const Selection: React.FC<Props> = ({ setAbc }) => {
    return (
        <div>
            <SkButton onButtonClicked={() => setAbc('A')}>A</SkButton>
            <SkButton onButtonClicked={() => setAbc('B')}>B</SkButton>
            <SkButton onButtonClicked={() => setAbc('C')}>C</SkButton>
        </div>
    );
};

export default Selection;
