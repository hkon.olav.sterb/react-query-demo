import { addTodo } from '../endpoints/todosEndpoints';
import { SkButton, SkTextInput } from '@statkraft/design-system-react';
import SingleTodo from '../components/SingleTodo';
import { useMutation } from '@tanstack/react-query';
import React from 'react';
import { useTodos } from '../queries/useTodos';

const TodoList: React.FC = () => {
    const [newTodoTitle, setNewTodoTitle] = React.useState('');
    const todoQuery = useTodos();

    const createTodoMutation = useMutation({
        mutationFn: (title: string) => {
            return addTodo(title);
        },
        onSuccess: (todo) => {
            console.log(todo);
            todoQuery.refetch();
            setNewTodoTitle('');
        },
    });

    const disableInput = createTodoMutation.isPending  || todoQuery.isLoading;
    return (
        <>
            <div className='new-todo-container'>
                <label>Add Todo</label>
                <SkTextInput
                    onChange={(e) => setNewTodoTitle(e.target.value)}
                    value={newTodoTitle}
                    onInputChanged={(newState) => setNewTodoTitle(newState)}
                    disabled={disableInput}
                />
                <SkButton disabled={disableInput} onButtonClicked={() => createTodoMutation.mutate(newTodoTitle)}>
                    Add
                </SkButton>
            </div>

            {todoQuery.isLoading ? (
                <h1>Loading...</h1>
            ) : (
                <div className='todo-container'>
                    {todoQuery.data?.map((todo) => <SingleTodo key={todo.id} todo={todo} />)}
                </div>
            )}
        </>
    );
};

export default TodoList;
