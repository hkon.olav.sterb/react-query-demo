import React from 'react';
import { TodoType, deleteTodo, updateTodo } from '../endpoints/todosEndpoints';
import { SkButton } from '@statkraft/design-system-react';
import { useMutation, useQueryClient } from '@tanstack/react-query';

type Props = {
    todo: TodoType;
};

const SingleTodo: React.FC<Props> = ({ todo }) => {
    const queryClient = useQueryClient();
    const updateTodoMutation = useMutation({
        mutationFn: (e: TodoType) => {
            return updateTodo(e);
        },
        onSuccess: () => {
            queryClient.invalidateQueries({ predicate: (query) => query.queryKey[0] === 'todo' });
        },
    });

    const deleteTodoMutation = useMutation({
        mutationFn: (id: number) => {
            return deleteTodo(id);
        },
        onSuccess: () => {
            queryClient.invalidateQueries({ predicate: (query) => query.queryKey[0] === 'todo' });
        },
    });
    const disableInput = updateTodoMutation.isPending || deleteTodoMutation.isPending;
    return (
        <React.Fragment key={todo.id}>
            <h6 className={todo.done ? 'todo-done' : undefined}>{todo.title}</h6>
            <input
                type='checkbox'
                checked={todo.done}
                onChange={() => updateTodoMutation.mutate({ ...todo, done: !todo.done })}
                disabled={disableInput}
            />
            <SkButton disabled={disableInput} onButtonClicked={() => deleteTodoMutation.mutate(todo.id)}>
                Delete
            </SkButton>
        </React.Fragment>
    );
};

export default SingleTodo;
