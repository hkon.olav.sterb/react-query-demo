function Home() {
    return (
        <div className='main-container'>
            <h1>React data fetching</h1>
            <ul>
                <li>
                    <a href='/naive'>Naive data fetching</a>
                </li>
                <li>
                    <a href='/effect'>Use effect data fetching</a>
                </li>
                <li>
                    <a href='/query'>Tan stack query data fetching</a>
                </li>
            </ul>
            <h1>Async State Management</h1>
            <ul>
                <li>
                    <a href='/todo'>React query showcase</a>
                </li>
            </ul>
        </div>
    );
}

export default Home;
