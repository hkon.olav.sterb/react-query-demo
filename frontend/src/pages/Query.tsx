import React from 'react';
import Selection from '../components/Selection';
import UseQueryDataFetch from '../components/UseQueryDataFetch';

const Query: React.FC = () => {
    const [abc, setAbc] = React.useState('');

    return (
        <div className='main-container'>
            <UseQueryDataFetch abc={abc} />
            <Selection setAbc={setAbc} />
        </div>
    );
};

export default Query;
