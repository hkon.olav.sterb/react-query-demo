import React from 'react';
import Selection from '../components/Selection';
import NaiveDataFetch from '../components/NaiveDataFetch';

const Naive: React.FC = () => {
    const [abc, setAbc] = React.useState('');

    return (
        <div className='main-container'>
            <NaiveDataFetch abc={abc} />
            <Selection setAbc={setAbc} />
        </div>
    );
};

export default Naive;
