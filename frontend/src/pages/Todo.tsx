import React from 'react';
import TodoList from '../components/TodoList';
import TodoCount from '../components/TodoCount';

const Todo: React.FC = () => {
    return (
        <div className='main-container'>
            <TodoList />
            <TodoCount />
        </div>
    );
};

export default Todo;
