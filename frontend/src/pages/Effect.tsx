import React from 'react';
import Selection from '../components/Selection';
import UseEffectDataFetch from '../components/UseEffectDataFetch';

const Effect: React.FC = () => {
    const [abc, setAbc] = React.useState('');

    return (
        <div className='main-container'>
            <UseEffectDataFetch abc={abc} />
            <Selection setAbc={setAbc} />
        </div>
    );
};

export default Effect;
